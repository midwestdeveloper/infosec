# Must do for your phone

A culture of caring about security and taking basic steps to protect each other reduces the potential for harm.

## Lock phone with unique passcode

Your phone should be locked and password-protected when you are not using it. This will keep other people from opening it and looking at your texts, what tabs you have open in your browser, your contacts, or the contents of your apps.

### Some guidelines for passwords:
  * Your password should be unique, not something you have used elsewhere.
  * Passwords should be 7+ characters in length. The longer the better, but don't make it _too_ much of a pain to unlock your phone.
  * The most secure passwords contain a mix of letters and numbers.
  * It is okay to use a numbers-only PIN passcode. Some people find a numbers-only keypad easier to use than a full letters-and-numbers keyboard. PIN passcodes should be:
    * 7+ characters in length
    * Set so you can't see how long they are
  * Do not use biometrics - e.g. fingerprints, iris scanning, or facial recognition - to unlock your phone.
  * It is a good idea to change your password periodically.

### Why locking your phone *with a password* matters
  * Your phone contains important information that could harm you and your comrades if it's seen by the wrong people. This data includes:
    * Texts/Signal messages
    * Emails
    * Logged in accounts (e.g. social media)
    * Contacts: most all the people you organize with are in your phone
    * Financial information (e.g. credit card info, banking)
    * Photos
  * "Pattern" passcodes (available on Android phones) are relatively easy to get through due to their simplicity
  * Biometrics can be faked - e.g. if you have your phone set to unlock with facial recognition, someone could just hold up a photo of you to get in.
  * Police can (legally, physically) compel you to unlock with face, fingerprint, etc.

### How to password-protect your phone
  * [Password-protect Android phone](https://www.samsung.com/au/support/mobile-devices/how-to-set-a-pattern-pin-or-password-on-your-lock-screen)
  * iPhone

## Auto-lock phone with timeout

Your phone should lock automatically after a short amount of time without you using it.

Why: if you happen to set your phone down without locking it, it will lock automatically within a set amount of time, preventing others from getting into it.

## Disable notifications on the lock screen

What: some phones display notifications - some containing sensitive data like text or email previews - when the phone is locked. 

Why:

  * Notifications can show a lot of information and metadata (who is contacting you when) even with the phone locked.

How:

  * Android
  * iPhone

## Signal

### Set up

  * Use a 6+ digit PIN and memorize it.
  * Enable registration lock (only after you have definitely memorized your Signal PIN, this prevents setting up a new Signal without the PIN.)

### Usage

  * Know who is in the loop.
  * If it's so large you don't immediately know who is in the loop, assume it is effectively public.
  * Leave and delete loops when done (two steps; first leave, then delete).


## Encrypt phone

  * Android
  * iPhone

## Safe practices

  * Do not install an app by clicking a link; go to the app store and search for the app you want to install.
  * Do not store new contacts to Google nor to SIM card, but (if adding a contact is needed at all) to phone.

## Other settings

  * Enable automatic software updates
  * Enable MAC Address Randomization
  * On iPhone, disable AirDrop.
  * Disable location tracking or remove apps that request location tracking permissions.

## Encrypt SIM Card

  * Android - https://www.howtogeek.com/259360/how-to-set-up-sim-card-lock-for-a-more-secure-android-phone/
  * iPhone

## Set up port validation with your carrier

Add a PIN or passcode to your wireless account with AT&T, Verizon, T-Mobile or other carrier.  This is something you will enter into their website or tell a company representative when getting a new phone or switching carriers, not something you enter into your phone.

https://www.thebalanceeveryday.com/prevent-your-mobile-number-from-being-ported-4160360

This should also make it less likely that your provider will let someone impersonate you to get your number on their phone, https://www.cnet.com/how-to/sim-swap-fraud-how-to-prevent-your-phone-number-from-being-stolen/

## When you do not want your location known

Ideally, you would leave your phone at home, or turn it off and put it in a Faraday bag.  When that is not practical:

  * Put into airplane mode
  * Enable Wi-Fi if data needed and trusted-ish network is available.
  * Disable bluetooth

# General

  * Never rely on technology.
  * Don't put anything in writing or a recording you wouldn't want in public or court.

# More information

https://ssd.eff.org/
