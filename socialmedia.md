People who want to dox you can get useful information from your social media accounts, but there are steps you can take to thwart them. You don't necessarily have to delete your accounts - often removing your name and making your posts private is enough. However, some situations may warrant deleting accounts. It's up to you to decide what makes you feel safest.

In this document, I've outlined the steps for making accounts on some common social media accounts more private. There are many other social media websites I do not cover. To help you with those, I’ve provided a checklist that you can use to help lock down any social media account. Start with the sites/apps covered below, then make a list of your other social media accounts. Use the checklist to go through each and see whether they have information that could make it easier for bad actors to find you.


### Facebook

1. Change your name to an alias: [Change Facebook Name](https://www.facebook.com/help/173909489329079)

2. Change your profile picture to one that does not contain you, people you associate with, or places you spend time at. Make sure you change your cover photo, too.

3. Limit your past posts to "friends" or "only me":[ Limit Past Facebook Posts](https://m.facebook.com/help/236898969688346/)

4. Make sure all your profile info (hometown, workplace, family, etc.) is set to "friends" or "only me"

5. In a web browser (i.e. Chrome or Safari), go to your profile and look at the URL. It should contain either an ID number (facebook.com/index.php?u=123456) or a username (facebook.com/lucy.parsons). If it's the latter, make sure it is not your real name. Anyone who finds your profile WILL be able to see it. Here is how to change your username: [Change Facebook Username](https://m.facebook.com/help/www/162586890471598https://m.facebook.com/help/www/162586890471598)

If needed, you can deactivate your account: [Deactivate Facebook Account](https://m.facebook.com/help/250563911970368)


### Twitter



1. Set your tweets to “protected.” This will keep people who aren't already followers from seeing your tweets: [Protecting Your Tweets](https://help.twitter.com/en/safety-and-security/how-to-make-twitter-private-and-public)
2.  Change your display name to an alias: [Change Twitter Display Name](https://help.twitter.com/en/managing-your-account/how-to-customize-your-profile)
3. If your @ (username) is your real name, change it to something else: [Changing Twitter Username](https://help.twitter.com/en/managing-your-account/change-twitter-handle)
4. Change your profile picture to one that does not contain you, people you associate with, or places you spend time at.
5. Check that your bio doesn’t have information that could be used to identify you.

You can also deactivate your account: [Deactivate Twitter Account](https://help.twitter.com/en/managing-your-account/how-to-deactivate-twitter-account)


### Instagram 



1. Set your account to private. This will prevent people who are not your followers from seeing your posts, and will prevent new people from following you: [Set Instagram Account to Private](https://help.instagram.com/448523408565555/)
2. If your name is your real name, change it to an alias: [Change Instagram Name](https://help.instagram.com/583107688369069/)
3. If your username contains your real name, change it: [Change Instagram Username](https://help.instagram.com/583107688369069/)
4. Change your profile picture to one that does not contain you, people you associate with, or places you spend time at.
5. Check that your bio doesn’t have information that could be used to identify you.

You can also deactivate your account: [Deactivate Instagram Account](https://help.instagram.com/728869160569983)


### LinkedIn

If you do not have an immediate, practical need for a LinkedIn account, my recommendation is to delete it. It is an easy way for bad actors to find out a lot of information about you, including where you work, your work history, and your professional connections. It is possible to make your account visible only to people with LinkedIn accounts, and block specific LinkedIn accounts from viewing your profile, but it is so easy to get around a block by making a new account that I don't think it offers much actual security. 

[Deleting your LinkedIn account](https://www.linkedin.com/help/linkedin/answer/63/closing-your-linkedin-account)


### Other Websites/Apps


#### Some commonly-used websites/apps



1. Snapchat
2. Tumblr
3. Reddit
4. TikTok
5. Google social media products (e.g. Blogger)


#### Checklist



1. Does your display name contain identifying information, such as name or hometown?
2. Does your username contain identifying information, such as name or birth year?
3. Does your profile picture contain you, people you associate with, or places you frequent?
4. Is your profile information or bio publicly visible? Does it contain information that could be used to identify you, such as real name, birth year, workplace, or links to other profiles?
5. Can people find you on the website/app by searching your email address or phone number?
6. Do you have posts that contain information that could be used to find you? Examples: name, workplace, bars/clubs you go to, place of worship, clubs or organizations you’re part of, names of children/friends/pets/partners, and school you or your child[ren] are enrolled in. It is usually okay to have posts like these, but it is safest to set them so only your _trusted_ friends can see them.
7. Can someone who is not your friend/follower see your posts?
8. Do you have friends or followers (people who can see your posts) you don’t know?
